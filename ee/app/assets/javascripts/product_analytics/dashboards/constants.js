import { s__, __ } from '~/locale';

export const I18N_DASHBOARD_LIST = {
  title: s__('ProductAnalytics|Product analytics dashboards'),
  description: s__(
    'ProductAnalytics|Dashboards are created by editing the projects dashboard files.',
  ),
  learnMore: __('Learn more.'),
};
